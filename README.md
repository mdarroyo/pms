# Pension Management System
여기에서 사이트의 해명 채우기 바람

## 전제 조건
1. Python 3.4 >=
2. virtualenv
3. MySQL
4. Node.js
5. Redis
6. Foundation CLI

## 맥용 환경 설치 과정
* (1) [Python 3](https://www.python.org/downloads/mac-osx/) 
* (2) virtualenv 터미널을 열어서 아래 명령이 참고하시며 실행하시기 바람
```bash
pip3 install virtualenv
```
* (3) [MySQL](https://dev.mysql.com/downloads/mysql/) 깔아주시고 데이터베이스 생성함
```bash
mysql -u root -p
```
```mysql
CREATE DATABASE staysuite CHARACTER SET utf8 COLLATE utf8_general_ci;
```
* (4) 남은 프로그램 편하게 깔 수 있도록 Homebrew 설치하세요. [Homebrew](https://brew.sh/index_ko)
* 터미널을 열어서 아래 명령이 참고하시며 실행하시기 바람
* Node.js 설치
```bash
brew doctor
brew update
brew install node
```

* (5) Redis 설치
```bash
brew install redis
brew services start redis
redis-cli ping
redis-server
```

* (6) Foundation 설치
```bash
npm install --global foundation-cli
```

## 환경 변수
```bash
export SS_DB_NAME=staysuite
export SS_DB_USER=root
export SS_DB_PASSWORD=secretpassword
```

## 프로잭트 세팅
우선은 소스코드 클론함
```bash
git clone https://gitlab.com/staysuite/pms/pms.git
```

파이썬 경우에는 필요할 페키지 전부 가상 환경에 설치하는게 매우 추천한다고 한다
```bash
cd pms
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
```

Foundation 프레임워크를 위해서 필요할 리소스들을 다운로드함
```bash
cd static/foundation
npm install
```

데이터베이스 설정
```bash
python manage.py migrate
```

## 로컬 실행
만약에 환경 세팅 정상적으로 설정 되어있으면 아래 명령만 실행하면 사이트 로컬에 launching 됨
```bash
python manage.py runserver
```

## 프런트엔드 리소스 파일 갱신
```bash
cd static/foundation
foundation watch
```

## 코딩 양식 가이드
* Python: [PEP 8 Style Guide](https://www.python.org/dev/peps/pep-0008/)
* SASS: (BEM)[http://getbem.com/naming/]