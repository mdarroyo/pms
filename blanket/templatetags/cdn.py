# blanket/templatetags/cdn.py

from django import template
from ..settings import *
import os

register = template.Library()


@register.simple_tag
def asset_url(path) -> str:
    load_remote = os.environ.get('SS_LOAD_REMOTE_ASSET', False)
    if load_remote:
        return CDN_LINK + '/static/' + path
    return '/static/' + path

