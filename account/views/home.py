# account/views/home.py

from django.views.generic import CreateView
from django.urls import reverse_lazy

from ..models.application import Application
from ..forms.apply import CreateApplicationForm


class Home(CreateView):
    """
    View for the welcome page
    """
    template_name = 'account/home.html'
    model = Application
    form_class = CreateApplicationForm
    success_url = reverse_lazy('home')
