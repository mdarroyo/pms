# account/managers.py

from django.contrib.auth.models import BaseUserManager


class SiteUserManager(BaseUserManager):
    """
    Custom user manager.
    """

    def create_user(self, username, email, password, **extra_fields):
        """
        When we create the user, we need to save both username and email in lowercase.

        :param username:
        :param email:
        :param password:
        :param extra_fields:
        :return:
        """
        if not email:
            raise ValueError('Email is required')
        email = self.normalize_email(email).lower()
        user = self.model(username=username.lower(), email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        """
        Create a super user.
        
        :param username:
        :param email:
        :param password:
        :param extra_fields:
        :return:
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        return self.create_user(username, email, password, **extra_fields)
