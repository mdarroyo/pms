# account/models/user.py

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models

from ..managers import SiteUserManager
from ..apps import AccountConfig


class SiteUser(AbstractBaseUser, PermissionsMixin):
    """
    Extension of the default user.
    """
    username = models.CharField(
        'username',
        max_length=150,
        unique=True,
        help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.',
        error_messages={
            'unique': "A user with that username already exists.",
        },
    )
    email = models.EmailField(unique=True, blank=False, max_length=255)
    date_joined = models.DateTimeField('Date Joined', auto_now_add=True)
    first_name = models.CharField(max_length=255, blank=True, verbose_name='First Name', default='')
    last_name = models.CharField(max_length=255, blank=True, verbose_name='Last Name', default='')
    USERNAME_FIELD = 'username'

    is_staff = models.BooleanField(
        'Staff Status',
        default=False,
        help_text='Is the user an administrator'
    )
    is_active = models.BooleanField(
        'Active',
        default=True,
        help_text='Is the user account active'
    )
    objects = SiteUserManager()

    def __str__(self):
        return self.email

    class Meta:
        db_table = AccountConfig.name + '_site_user'
