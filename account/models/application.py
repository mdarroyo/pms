# account/models/application.py

from django.db import models


class Application(models.Model):
    location = models.CharField(max_length=255)
    suite_type = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    website = models.URLField(max_length=255)

