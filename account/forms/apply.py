# account/forms/apply.py

from django import forms

from ..models.application import Application


class CreateApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = '__all__'
